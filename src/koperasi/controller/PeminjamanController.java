/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package koperasi.controller;

import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Random;
import javax.swing.JOptionPane;
import koperasi.model.PeminjamanModel;
import koperasi.view.PeminjamanView;

/**
 *
 * @author Lucky
 */
public class PeminjamanController {
    private PeminjamanModel model;
    SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");  
    public void setModel(PeminjamanModel model){
        this.model = model;
    }
    
    public void resetForm(PeminjamanView view){
        String uang = view.getPeminjamanUang().getText();
        String jangka_waktu = view.getJangkaWaktu().getText();
        if(uang.equals("") || jangka_waktu.equals("")){
            
        }else{
            view.getPeminjamanUang().setText("");
            view.getJangkaWaktu().setText("");
            model.resetForm();
        }
    }
    public void simpanForm(PeminjamanView view) throws SQLException{
        String jumlah_uang = view.getPeminjamanUang().getText();
        String jangka_waktu = view.getJangkaWaktu().getText();
        String peminjaman_id = this.generateId();
        String tanggal_masuk = dateFormat.format(view.getDate().getDate());
        int bunga =  (int) (0.05 * Integer.parseInt(jumlah_uang)) ;
        int bayaran_perbulan = (Integer.parseInt(jumlah_uang) / Integer.parseInt(jangka_waktu)) + bunga;
        int sisa_hutang = bayaran_perbulan * Integer.parseInt(jangka_waktu);
        if(jumlah_uang.trim().equals("")){
            JOptionPane.showMessageDialog(view,"Jumlah uang kosong!");
        }else if(jangka_waktu.trim().equals("")){
            JOptionPane.showMessageDialog(view,"Jangka waktu kosong!");
        }else{
            model.setPeminjaman_id(peminjaman_id);
            model.setJumlah_uang(jumlah_uang);
            model.setTanggal_masuk(tanggal_masuk);
            model.setJangka_waktu(jangka_waktu);
            model.setBunga(bunga);
            model.setBayaran_perbulan(bayaran_perbulan);
            model.setSisa_hutang(sisa_hutang);
            model.simpanForm();
            view.getPeminjamanInformation().setModel(model.loadTablePeminjaman());
            this.resetForm(view);
        }
    }
    
    public void updateHutang(PeminjamanView view) throws SQLException{
        String uangBayar = view.getPeminjamanBayar().getText();
        model.setBayar(uangBayar);
        model.updateHutang();
        view.getPeminjamanInformation().setModel(model.loadTablePeminjaman());
        this.resetForm(view);
        view.getPeminjamanBayar().setText("");
        view.getLabelId().setText("");
    }
    
    public void searchMember(PeminjamanView view) throws SQLException{
        String keywordNama = view.getSearchFieldKeyword().getText();
        model.setSearchKeyword(keywordNama);
        view.getMemberInformation().setModel(model.searchMember());        
    }
    
    protected String generateId() {
      String SALTCHARS = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz1234567890";
      StringBuilder salt = new StringBuilder();
      Random rnd = new Random();
      while (salt.length() < 6) { // length of the random string.
          int index = (int) (rnd.nextFloat() * SALTCHARS.length());
          salt.append(SALTCHARS.charAt(index));
      }
      String saltStr = salt.toString();
      return saltStr;

    }
}
