/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package koperasi.controller;
import java.sql.SQLException;
import koperasi.model.MemberModel;
import koperasi.view.MemberView;
import java.time.format.DateTimeFormatter;  
import java.time.LocalDateTime;   
import java.util.Random;
import javax.swing.JOptionPane;
/**
 *
 * @author root
 */
public class MemberController {
    private MemberModel model;
    
    public void setModel(MemberModel model){
        this.model = model;
    }
    
    public void resetForm(MemberView view){
        model.resetForm();
        view.getMemberName().setText("");
        view.getMemberPhone().setText("");
        view.getMemberEmail().setText("");
        view.getMemberAddress().setText("");
        view.getMemberSimpananPokok().setText("");
    }
    
    public void simpanForm(MemberView view) throws SQLException{
        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy-MM-dd hh:mm:ss");  
        LocalDateTime now = LocalDateTime.now();  
        
        String member_name = view.getMemberName().getText();
        String member_phone = view.getMemberPhone().getText();
        String member_email = view.getMemberEmail().getText();
        String member_address = view.getMemberAddress().getText();
        String member_simpanan_pokok = view.getMemberSimpananPokok().getText();
        String member_join_date = dtf.format(now);
        String member_id = this.generateId();
        String simpanan_pokok_id = this.generateId();
        if(member_name.equals("")){
            JOptionPane.showMessageDialog(null, "Field nama harus diisi");
        }else if(member_address.equals("")){
            JOptionPane.showMessageDialog(null, "Field alamat harus diisi");
        }else{  
            model.setMember_name(member_name.trim());
            model.setMember_phone(member_phone.trim());
            model.setMember_email(member_email.trim());
            model.setMember_address(member_address.trim());
            model.setMember_join_date(member_join_date.trim());
            model.setMember_id(member_id);
            model.setSimpanan_pokok_id(simpanan_pokok_id);
            model.setMember_simpanan_pokok(member_simpanan_pokok);
            model.simpanForm();
            view.getMemberInformation().setModel(model.loadTable());
            this.resetForm(view);
        }
    }
    
    public void updateForm(MemberView view) throws SQLException{
        String member_name = view.getMemberName().getText();
        String member_phone = view.getMemberPhone().getText();
        String member_email = view.getMemberEmail().getText();
        String member_address = view.getMemberAddress().getText();
        model.setMember_name(member_name.trim());
        model.setMember_phone(member_phone.trim());
        model.setMember_email(member_email.trim());
        model.setMember_address(member_address.trim());
        model.updateForm();
        view.getMemberInformation().setModel(model.loadTable());
        this.resetForm(view);
    }
    
    public void deleteForm(MemberView view) throws SQLException{
        model.deleteForm();
        view.getMemberInformation().setModel(model.loadTable());
        this.resetForm(view);
    }
    
    protected String generateId() {
        String SALTCHARS = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz1234567890";
        StringBuilder salt = new StringBuilder();
        Random rnd = new Random();
        while (salt.length() < 6) { // length of the random string.
            int index = (int) (rnd.nextFloat() * SALTCHARS.length());
            salt.append(SALTCHARS.charAt(index));
        }
        String saltStr = salt.toString();
        return saltStr;

    }
    
}
