/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package koperasi.controller;

import java.sql.SQLException;
import javax.swing.JOptionPane;

import koperasi.model.LoginModel;
import koperasi.view.LoginView;
import koperasi.view.MainFrame;
/**
 *
 * @author USER
 */
public class LoginController {
    private LoginModel model;
   
    
    public void setModel(LoginModel model){
        this.model = model;
    }
    
    public void login(LoginView view, MainFrame main) throws SQLException{
        String email = view.getEmail().getText();
        String password = view.getPassword().getText();
        
        if(email.trim().equals("")){
            JOptionPane.showMessageDialog(view, "Email Belum Diisi !");
        }else if(password.equals("")){
            JOptionPane.showMessageDialog(view, "Password Belum Diisi !");
        }else{
            model.setEmail(email);
            model.setPassword(password);
            model.login(view, main);
        }
    }
}
