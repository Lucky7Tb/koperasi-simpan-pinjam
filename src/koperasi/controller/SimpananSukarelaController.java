/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package koperasi.controller;

import java.sql.SQLException;
import java.text.SimpleDateFormat;
import javax.swing.JOptionPane;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Random;
import koperasi.model.SimpananSukarelaModel;
import koperasi.view.SimpananSukarelaView;
/**
 *
 * @author hajjah
 */
public class SimpananSukarelaController {
    private SimpananSukarelaModel model;
    SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
    public void setModel(SimpananSukarelaModel model){
        this.model = model;
    }
    
    public void resetForm(SimpananSukarelaView view){
        String uang = view.getTxtuang().getText();
        
        if(uang.equals("")){
            
        }else{
            view.getTxtuang().setText("");
            model.resetForm();
        }
    }
    
     public void searchMember(SimpananSukarelaView view) throws SQLException{
        String keywordNama = view.getSearchFieldKeyword().getText();
        model.setSearchKeyword(keywordNama);
        view.getMemberInformation().setModel(model.searchMember());        
    }
    
    public void simpanForm(SimpananSukarelaView view) throws SQLException{
        String simpanan_sukarela = view.getTxtuang().getText();
        String simpanan_sukarela_id = this.generateId();
        String tanggal_masuk = dateFormat.format(view.getDate().getDate());
        
        if(simpanan_sukarela.trim().equals("")){
            JOptionPane.showMessageDialog(view,"Jumlah Uang Kosong!");
        }else{
            model.setSimpanan_sukarela_id(simpanan_sukarela_id);
            model.setSimpananSukarela(simpanan_sukarela);
            model.setTanggal_masuk(tanggal_masuk);
            model.simpanForm();
            view.getSimpananInformation().setModel(model.loadTableSimpanan());
            this.resetForm(view);
        }
    }
    
      protected String generateId() {
        String SALTCHARS = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz1234567890";
        StringBuilder salt = new StringBuilder();
        Random rnd = new Random();
        while (salt.length() < 6) { // length of the random string.
            int index = (int) (rnd.nextFloat() * SALTCHARS.length());
            salt.append(SALTCHARS.charAt(index));
        }
        String saltStr = salt.toString();
        return saltStr;

    }
}
