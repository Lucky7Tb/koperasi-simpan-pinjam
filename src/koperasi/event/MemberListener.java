/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package koperasi.event;

import koperasi.model.MemberModel;

/**
 *
 * @author Lucky
 */
public interface MemberListener {
    public void onChange(MemberModel member);
}
