/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package koperasi.view;

import java.sql.SQLException;
import koperasi.database.DatabaseConnection;

/**
 *
 * @author Lucky
 */
public class MainFrame extends javax.swing.JFrame {

    LoginView login = new LoginView();
    
    public MainFrame() {
        initComponents();
        MainPanel.add(new HomeView(600, 600));
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jXPanel1 = new org.jdesktop.swingx.JXPanel();
        flatButton1 = new com.mommoo.flat.button.FlatButton();
        flatButton2 = new com.mommoo.flat.button.FlatButton();
        jLabel1 = new javax.swing.JLabel();
        flatButton3 = new com.mommoo.flat.button.FlatButton();
        flatButton4 = new com.mommoo.flat.button.FlatButton();
        flatButton6 = new com.mommoo.flat.button.FlatButton();
        flatButton7 = new com.mommoo.flat.button.FlatButton();
        flatButton5 = new com.mommoo.flat.button.FlatButton();
        MainPanel = new org.jdesktop.swingx.JXPanel();
        jLabel2 = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        getContentPane().setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jXPanel1.setBackground(new java.awt.Color(0, 146, 63));
        jXPanel1.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        flatButton1.setBackground(new java.awt.Color(0, 146, 63));
        flatButton1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/assets/Staff_24px.png"))); // NOI18N
        flatButton1.setText("Member");
        flatButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                flatButton1ActionPerformed(evt);
            }
        });
        jXPanel1.add(flatButton1, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 210, 126, 60));

        flatButton2.setBackground(new java.awt.Color(0, 146, 63));
        flatButton2.setIcon(new javax.swing.ImageIcon(getClass().getResource("/assets/Home_24px.png"))); // NOI18N
        flatButton2.setText("Home");
        flatButton2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                flatButton2ActionPerformed(evt);
            }
        });
        jXPanel1.add(flatButton2, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 140, 126, 60));

        jLabel1.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/assets/logo-koperasi-13099.png"))); // NOI18N
        jXPanel1.add(jLabel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 20, 150, 110));

        flatButton3.setBackground(new java.awt.Color(0, 146, 63));
        flatButton3.setIcon(new javax.swing.ImageIcon(getClass().getResource("/assets/icons8_Paper_Money_24px.png"))); // NOI18N
        flatButton3.setText("Sukarela");
        flatButton3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                flatButton3ActionPerformed(evt);
            }
        });
        jXPanel1.add(flatButton3, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 270, 126, 60));

        flatButton4.setBackground(new java.awt.Color(0, 146, 63));
        flatButton4.setIcon(new javax.swing.ImageIcon(getClass().getResource("/assets/icons8_Paper_Money_24px.png"))); // NOI18N
        flatButton4.setText("Wajib");
        flatButton4.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                flatButton4ActionPerformed(evt);
            }
        });
        jXPanel1.add(flatButton4, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 340, 126, 60));

        flatButton6.setBackground(new java.awt.Color(0, 146, 63));
        flatButton6.setIcon(new javax.swing.ImageIcon(getClass().getResource("/assets/icons8-downloading-updates-24.png"))); // NOI18N
        flatButton6.setText("Laporan");
        flatButton6.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                flatButton6ActionPerformed(evt);
            }
        });
        jXPanel1.add(flatButton6, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 470, 126, 60));

        flatButton7.setBackground(new java.awt.Color(0, 146, 63));
        flatButton7.setIcon(new javax.swing.ImageIcon(getClass().getResource("/assets/icons8-shutdown-24(1).png"))); // NOI18N
        flatButton7.setText("Logout");
        flatButton7.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                flatButton7ActionPerformed(evt);
            }
        });
        jXPanel1.add(flatButton7, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 540, 126, 60));

        flatButton5.setBackground(new java.awt.Color(0, 146, 63));
        flatButton5.setIcon(new javax.swing.ImageIcon(getClass().getResource("/assets/icons8_Paper_Money_24px.png"))); // NOI18N
        flatButton5.setText("Peminjaman");
        flatButton5.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                flatButton5ActionPerformed(evt);
            }
        });
        jXPanel1.add(flatButton5, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 410, 126, 60));

        getContentPane().add(jXPanel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, -1, 600));

        jLabel2.setFont(new java.awt.Font("Bodoni MT", 0, 36)); // NOI18N
        jLabel2.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel2.setText("Welcome");

        javax.swing.GroupLayout MainPanelLayout = new javax.swing.GroupLayout(MainPanel);
        MainPanel.setLayout(MainPanelLayout);
        MainPanelLayout.setHorizontalGroup(
            MainPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(MainPanelLayout.createSequentialGroup()
                .addGap(154, 154, 154)
                .addComponent(jLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 234, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(212, Short.MAX_VALUE))
        );
        MainPanelLayout.setVerticalGroup(
            MainPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(MainPanelLayout.createSequentialGroup()
                .addGap(19, 19, 19)
                .addComponent(jLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 90, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(491, Short.MAX_VALUE))
        );

        getContentPane().add(MainPanel, new org.netbeans.lib.awtextra.AbsoluteConstraints(150, 0, 600, 600));

        pack();
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    private void flatButton4ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_flatButton4ActionPerformed
         try {
            MainPanel.removeAll();
            MainPanel.repaint();
            MainPanel.revalidate();

            MainPanel.add(new SimpananWajibView(600, 600));
            MainPanel.repaint();
            MainPanel.revalidate();
        } catch (SQLException ex) {
            System.out.println(ex);
        }
    }//GEN-LAST:event_flatButton4ActionPerformed

    private void flatButton5ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_flatButton5ActionPerformed
       try {
            MainPanel.removeAll();
            MainPanel.repaint();
            MainPanel.revalidate();

            MainPanel.add(new PeminjamanView(600, 600));
            MainPanel.repaint();
            MainPanel.revalidate();
        } catch (SQLException ex) {
            System.out.println(ex);
        }
    }//GEN-LAST:event_flatButton5ActionPerformed

    private void flatButton6ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_flatButton6ActionPerformed
        MainPanel.removeAll();
        MainPanel.repaint();
        MainPanel.revalidate();

        MainPanel.add(new ReportView(600, 600));
        MainPanel.repaint();
        MainPanel.revalidate();       
    }//GEN-LAST:event_flatButton6ActionPerformed

    private void flatButton7ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_flatButton7ActionPerformed
        this.dispose();
        login.setVisible(true);
    }//GEN-LAST:event_flatButton7ActionPerformed

    private void flatButton2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_flatButton2ActionPerformed
        MainPanel.removeAll();
        MainPanel.repaint();
        MainPanel.revalidate();

        MainPanel.add(new HomeView(600, 600));
        MainPanel.repaint();
        MainPanel.revalidate();       
    }//GEN-LAST:event_flatButton2ActionPerformed

    private void flatButton1ActionPerformed(java.awt.event.ActionEvent evt) {// GEN-FIRST:event_flatButton1ActionPerformed
        try {
            MainPanel.removeAll();
            MainPanel.repaint();
            MainPanel.revalidate();

            MainPanel.add(new MemberView(600, 600));
            MainPanel.repaint();
            MainPanel.revalidate();
        } catch (SQLException ex) {
            System.out.println(ex);
        }
    }// GEN-LAST:event_flatButton1ActionPerformed

    private void flatButton3ActionPerformed(java.awt.event.ActionEvent evt) {// GEN-FIRST:event_flatButton3ActionPerformed
        try{
            MainPanel.removeAll();
            MainPanel.repaint();
            MainPanel.revalidate();

            MainPanel.add(new SimpananSukarelaView(600, 600));
            MainPanel.repaint();
            MainPanel.revalidate();
       
        }catch(SQLException ex){
            System.out.println(ex);
        }

    }// GEN-LAST:event_flatButton3ActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        // <editor-fold defaultstate="collapsed" desc=" Look and feel setting code
        // (optional) ">
        /*
         * If Nimbus (introduced in Java SE 6) is not available, stay with the default
         * look and feel. For details see
         * http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(MainFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(MainFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(MainFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(MainFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        // </editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                DatabaseConnection.RunConnection();
                new MainFrame().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private org.jdesktop.swingx.JXPanel MainPanel;
    private com.mommoo.flat.button.FlatButton flatButton1;
    private com.mommoo.flat.button.FlatButton flatButton2;
    private com.mommoo.flat.button.FlatButton flatButton3;
    private com.mommoo.flat.button.FlatButton flatButton4;
    private com.mommoo.flat.button.FlatButton flatButton5;
    private com.mommoo.flat.button.FlatButton flatButton6;
    private com.mommoo.flat.button.FlatButton flatButton7;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private org.jdesktop.swingx.JXPanel jXPanel1;
    // End of variables declaration//GEN-END:variables
}
