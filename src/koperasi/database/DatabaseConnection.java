/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package koperasi.database;

import java.sql.DriverManager;
import java.sql.Connection;
import java.sql.Statement;
import java.sql.ResultSet;
import java.sql.SQLException;
/**
 *
 * @author Lucky
 */
public class DatabaseConnection {
    static final String JDBC_DRIVER = "com.mysql.jdbc.Driver";
    protected static final String DB_URL = "jdbc:mysql://localhost/koperasi";
    protected static final String USER = "root";
    protected static final String PASS = "";
    
    
    public static Connection connection;
    public static Statement statement;
    public static ResultSet result;
    
     public static void RunConnection(){
         try {
            Class.forName(JDBC_DRIVER);
            connection = DriverManager.getConnection(DB_URL, USER, PASS);
            statement = connection.createStatement();
            System.out.println("Koneksi berhasil");
        } catch (ClassNotFoundException | SQLException error) {
            System.out.println("Koneksi Gagal" + error.getMessage() );
        }
         
    }
}
