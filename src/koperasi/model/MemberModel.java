/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package koperasi.model;
import koperasi.event.MemberListener;

import java.sql.SQLException;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;

import static koperasi.database.DatabaseConnection.result;
import static koperasi.database.DatabaseConnection.statement;
/**
 *
 * @author root
 */
public class MemberModel {
    private String member_id;
    private String member_name;
    private String member_phone;
    private String member_email;
    private String member_address;
    private String member_join_date;
    private String member_simpanan_pokok;
    private String simpanan_pokok_id;

    private MemberListener memberListener;
    DefaultTableModel model = new DefaultTableModel();  
    
    public MemberModel(){
        model.addColumn("Id");
        model.addColumn("Nama");
        model.addColumn("Telpon");
        model.addColumn("Email");
        model.addColumn("Alamat");
        model.addColumn("Tanggal bergabung"); 
    }
    
    public String getMember_id() {
        return member_id;
    }

    public void setMember_id(String member_id) {
        this.member_id = member_id;
    }

    public String getMember_name() {
        return member_name;
    }

    public void setMember_name(String member_name) {
        this.member_name = member_name;
        fireOnChange();
    }

    public String getMember_phone() {
        return member_phone;
    }

    public void setMember_phone(String member_phone) {
        this.member_phone = member_phone;
        fireOnChange();
    }

    public String getMember_email() {
        return member_email;
    }

    public void setMember_email(String member_email) {
        this.member_email = member_email;
        fireOnChange();
    }

    public String getMember_address() {
        return member_address;
    }

    public void setMember_address(String member_address) {
        this.member_address = member_address;
        fireOnChange();
    }

    public String getMember_join_date() {
        return member_join_date;
    }

    public void setMember_join_date(String member_join_date) {
        this.member_join_date = member_join_date;
        fireOnChange();
    }
    
    public String getMember_simpanan_pokok() {
        return member_simpanan_pokok;
    }

    public void setMember_simpanan_pokok(String member_simpanan_pokok) {
        this.member_simpanan_pokok = member_simpanan_pokok;
    }
    
     public String getSimpanan_pokok_id() {
        return simpanan_pokok_id;
    }

    public void setSimpanan_pokok_id(String simpanan_pokok_id) {
        this.simpanan_pokok_id = simpanan_pokok_id;
    }
      public MemberListener getMemberListener(){
        return memberListener;
    }
    public void setMemberListener(MemberListener memberListener) {
        this.memberListener = memberListener;
    }
  
    
    protected void fireOnChange(){
        if(memberListener != null){
            memberListener.onChange(this);
        }
    }
    
    public DefaultTableModel loadTable() throws SQLException{
        String Query =  "SELECT * FROM member ORDER BY member_join_date DESC";
        result = statement.executeQuery(Query);
        model.setNumRows(0);
        while(result.next()){
            model.addRow(new Object[]{result.getString(1),result.getString(2),result.getString(3),result.getString(4),result.getString(5),result.getString(6)});
        }
        return model;
    }
    
     public void resetForm(){
        this.setMember_name("");
        this.setMember_phone("");
        this.setMember_address("");
        this.setMember_email("");
        this.setMember_id("");
        this.setMember_join_date("");
    }
     
     public void simpanForm() throws SQLException{
         System.out.println(this.getMember_simpanan_pokok());
        String Query = "INSERT INTO member (member_id, member_name, member_phone, member_email, member_address, member_join_date) VALUES ('%s', '%s', '%s', '%s', '%s', '%s')";
        Query = String.format(Query, this.getMember_id(), this.getMember_name(), this.getMember_phone(), this.getMember_email(), this.getMember_address(), this.getMember_join_date());
        if(!statement.execute(Query)){
            Query = "INSERT INTO simpanan_pokok (simpanan_pokok_id, member_id, simpanan_pokok) VALUE('%s', '%s', '%s')";
            if(this.getMember_simpanan_pokok() == null || this.getMember_simpanan_pokok().equals("")){
                  Query = String.format(Query, this.getSimpanan_pokok_id(), this.getMember_id(), 750000);
            }else{
                  Query = String.format(Query, this.getSimpanan_pokok_id(), this.getMember_id(), this.getMember_simpanan_pokok());
            }
            if(!statement.execute(Query)){
                JOptionPane.showMessageDialog(null, "Berhasil Disimpan");
            }
        }else{
            JOptionPane.showMessageDialog(null, "Gagal Disimpan");
        }
    }
     
     public void updateForm() throws SQLException{
         String Query = "UPDATE member set member_name = '%s', member_phone = '%s', member_email = '%s', member_address = '%s' WHERE member_id = '%s' ";
         Query = String.format(Query, this.getMember_name(), this.getMember_phone(), this.getMember_email(), this.getMember_address(), this.getMember_id());
          if(!statement.execute(Query)){
            JOptionPane.showMessageDialog(null, "Berhasil Diupdate");
        }else{
            JOptionPane.showMessageDialog(null, "Gagal Diupdate");
        }
     }
     
     public void deleteForm() throws SQLException{
         String Query = "DELETE FROM member WHERE member_id = '%s' ";
         Query = String.format(Query, this.getMember_id());
          if(!statement.execute(Query)){
            JOptionPane.showMessageDialog(null, "Berhasil Didelete");
        }else{
            JOptionPane.showMessageDialog(null, "Gagal Didelete");
        }
     }
    
}
