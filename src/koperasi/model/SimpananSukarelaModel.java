/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package koperasi.model;

import java.sql.SQLException;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;
import static koperasi.database.DatabaseConnection.result;
import static koperasi.database.DatabaseConnection.statement;
import koperasi.event.SimpananSukarelaListener;

/**
 *
 * @author hajjah
 */
public class SimpananSukarelaModel {
    private String simpanan_sukarela_id;
    private String member_id;
    private String simpanan_sukarela;
    private String SearchKeyword;
    private String tanggal_masuk;
    
    private SimpananSukarelaListener SimpananSukarelaListener;
    DefaultTableModel modelTableMember = new DefaultTableModel();
    DefaultTableModel modelTableSimpanan = new DefaultTableModel();
    
    public SimpananSukarelaModel(){
        modelTableMember.addColumn("Id");
        modelTableMember.addColumn("Nama");
        
        modelTableSimpanan.addColumn("Id");
        modelTableSimpanan.addColumn("Member Id");
        modelTableSimpanan.addColumn("Simpanan");
        modelTableSimpanan.addColumn("Tanggal Masuk");
    }
      
    public SimpananSukarelaListener getSimpananSukarelaListener(){
        return SimpananSukarelaListener;
    }
    
    public void setSimpananSukarelaListener(SimpananSukarelaListener SimpananSukarelaListener){
        this.SimpananSukarelaListener = SimpananSukarelaListener;
    }

    public String getSimpanan_sukarela_id() {
        return simpanan_sukarela_id;
    }

    public void setSimpanan_sukarela_id(String simpanan_sukarela_id) {
        this.simpanan_sukarela_id = simpanan_sukarela_id;
    }

    public String getMember_id() {
        return member_id;
    }

    public void setMember_id(String member_id) {
        this.member_id = member_id;
    }

    public String getTanggal_masuk() {
        return tanggal_masuk;
    }

    public void setTanggal_masuk(String tanggal_masuk) {
        this.tanggal_masuk = tanggal_masuk;
    }
    
    public String getSimpananSukarela() {
        return simpanan_sukarela;
    }

    public void setSimpananSukarela(String simpanan_sukarela) {
        this.simpanan_sukarela = simpanan_sukarela;
        fireOnChange();
    }
    
     public String getSearchKeyword() {
        return SearchKeyword;
    }

    public void setSearchKeyword(String SearchKeyword) {
        this.SearchKeyword = SearchKeyword;
        fireOnChange();
    }
    
    public DefaultTableModel searchMember() throws SQLException{
        String Query =  "SELECT * FROM member WHERE member_name LIKE '%"+this.getSearchKeyword()+"%' ";
        result = statement.executeQuery(Query);
        modelTableMember.setNumRows(0);
        while(result.next()){
            modelTableMember.addRow(new Object[]{result.getString(1),result.getString(2)});
        }
        return modelTableMember;
    }

    
    protected void fireOnChange(){
    if(SimpananSukarelaListener != null)
        {
            SimpananSukarelaListener.onChange(this);
        }
    }
    
     public DefaultTableModel loadTableMember() throws SQLException{
        String Query =  "SELECT * FROM member ORDER BY member_join_date DESC";
        result = statement.executeQuery(Query);
        modelTableMember.setNumRows(0);
        while(result.next()){
            modelTableMember.addRow(new Object[]{result.getString(1),result.getString(2)});
        }
        return modelTableMember;
    }
     
     public DefaultTableModel loadTableSimpanan() throws SQLException{
        String Query =  "SELECT * FROM simpanan_sukarela ORDER BY created_at DESC";
        result = statement.executeQuery(Query);
        modelTableSimpanan.setNumRows(0);
        while(result.next()){
            modelTableSimpanan.addRow(new Object[]{result.getString(1),result.getString(2),result.getString(3),result.getString(4)});
        }
        return modelTableSimpanan;
    }
    
    public void resetForm(){
        setSimpananSukarela("");
        setTanggal_masuk("");
        setMember_id("");
        setSimpanan_sukarela_id("");
    }
    public void simpanForm() throws SQLException{
        String Query = "INSERT INTO simpanan_sukarela (simpanan_sukarela_id, member_id, simpanan_sukarela, created_at) VALUE('%s', '%s', '%s', '%s')";
        Query = String.format(Query, this.getSimpanan_sukarela_id(), this.getMember_id(), this.getSimpananSukarela(), this.getTanggal_masuk());
        if(!statement.execute(Query)){
            JOptionPane.showMessageDialog(null, "Berhasil Disimpan");
        }
        resetForm();
    }
}
