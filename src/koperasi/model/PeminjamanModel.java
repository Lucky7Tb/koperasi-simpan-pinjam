/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package koperasi.model;

import java.sql.SQLException;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;
import static koperasi.database.DatabaseConnection.result;
import static koperasi.database.DatabaseConnection.statement;
import koperasi.event.PeminjamanListener;

/**
 *
 * @author Lucky
 */
public class PeminjamanModel {
    private String peminjaman_id;
    private String member_id;
    private String jumlah_uang;
    private String jangka_waktu;
    private String tanggal_masuk;
    private String status;
    private String bayar;
    private String SearchKeyword;
    private int bayaran_perbulan;
    private int sisa_hutang;
    private int bunga;
    
    private PeminjamanListener PeminjamanListener;
    DefaultTableModel modelTableMember = new DefaultTableModel();
    DefaultTableModel modelTablePinjaman = new DefaultTableModel();
    private PeminjamanListener peminjamanListener;
    
    public PeminjamanModel(){
        modelTableMember.addColumn("Id");
        modelTableMember.addColumn("Nama");
        
        modelTablePinjaman.addColumn("Id");
        modelTablePinjaman.addColumn("Member id");
        modelTablePinjaman.addColumn("Jumlah uang");
        modelTablePinjaman.addColumn("Bunga");
        modelTablePinjaman.addColumn("Jangka waktu");
        modelTablePinjaman.addColumn("Bayar perbulan");
        modelTablePinjaman.addColumn("Sisa hutang");
        modelTablePinjaman.addColumn("Tanggal masuk");
        modelTablePinjaman.addColumn("Status");
    }

    public String getMember_id() {
        return member_id;
    }

    public void setMember_id(String member_id) {
        this.member_id = member_id;
    }

    public String getPeminjaman_id() {
        return peminjaman_id;
    }

    public void setPeminjaman_id(String peminjaman_id) {
        this.peminjaman_id = peminjaman_id;
    }

    public String getJumlah_uang() {
        return jumlah_uang;
    }

    public void setJumlah_uang(String jumlah_uang) {
        this.jumlah_uang = jumlah_uang;
        fireOnChange();
    }

    public String getJangka_waktu() {
        return jangka_waktu;
    }

    public void setJangka_waktu(String jangka_waktu) {
        this.jangka_waktu = jangka_waktu;
        fireOnChange();
    }

    public String getTanggal_masuk() {
        return tanggal_masuk;
    }

    public void setTanggal_masuk(String tanggal_masuk) {
        this.tanggal_masuk = tanggal_masuk;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public int getBunga() {
        return bunga;
    }

    public void setBunga(int bunga) {
        this.bunga = bunga;
    }

    public int getBayaran_perbulan() {
        return bayaran_perbulan;
    }

    public void setBayaran_perbulan(int bayaran_perbulan) {
        this.bayaran_perbulan = bayaran_perbulan;
    }
    
    public PeminjamanListener getPeminjamanListener(){
        return peminjamanListener;
    }
    public void setPeminjamanListener(PeminjamanListener peminjamanListener) {
        this.peminjamanListener = peminjamanListener;
    }

    public String getBayar() {
        return bayar;
    }

    public void setBayar(String bayar) {
        this.bayar = bayar;
        fireOnChange();
    }

    public int getSisa_hutang() {
        return sisa_hutang;
    }

    public void setSisa_hutang(int sisa_hutang) {
        this.sisa_hutang = sisa_hutang;
    }

    public String getSearchKeyword() {
        return SearchKeyword;
    }

    public void setSearchKeyword(String SearchKeyword) {
        this.SearchKeyword = SearchKeyword;
        fireOnChange();
    }
    
    public DefaultTableModel searchMember() throws SQLException{
        String Query =  "SELECT * FROM member WHERE member_name LIKE '%"+this.getSearchKeyword()+"%' ";
        result = statement.executeQuery(Query);
        modelTableMember.setNumRows(0);
        while(result.next()){
            modelTableMember.addRow(new Object[]{result.getString(1),result.getString(2)});
        }
        return modelTableMember;
    }

    protected void fireOnChange(){
    if(PeminjamanListener != null)
        {
            PeminjamanListener.onChange(this);
        }
    }
    
    public DefaultTableModel loadTableMember() throws SQLException{
        String Query =  "SELECT * FROM member ORDER BY member_join_date DESC";
        result = statement.executeQuery(Query);
        modelTableMember.setNumRows(0);
        while(result.next()){
            modelTableMember.addRow(new Object[]{result.getString(1),result.getString(2)});
        }
        return modelTableMember;
    }
     
     public DefaultTableModel loadTablePeminjaman() throws SQLException{
        String Query =  "SELECT * FROM  peminjaman ORDER BY created_at DESC";
        result = statement.executeQuery(Query);
        modelTablePinjaman.setNumRows(0);
        while(result.next()){
            modelTablePinjaman.addRow(new Object[]{result.getString(1),result.getString(2),result.getInt(3),result.getInt(4),result.getString(5),result.getInt(6),result.getString(7), result.getString(8), result.getString(9)});
        }
        return modelTablePinjaman;
    }
    
    public void resetForm(){
        setJumlah_uang("");
        setTanggal_masuk("");
        setMember_id("");
        setPeminjaman_id("");
        setBunga(0);
        setJangka_waktu("");
        setStatus("");
        setBayaran_perbulan(0);
        setSisa_hutang(0);
        setBayar("");
    }
    public void simpanForm() throws SQLException{
        String Query = "INSERT INTO peminjaman (peminjaman_id, member_id, jumlah_uang, bunga, jangka_waktu, uang_per_bulan , sisa_hutang ,created_at) VALUE('%s', '%s', '%s', '%d', '%s', '%d', '%d', '%s')";
        Query = String.format(Query, this.getPeminjaman_id(), this.getMember_id(), this.getJumlah_uang(), this.getBunga(), this.getJangka_waktu(), this.getBayaran_perbulan(), this.getSisa_hutang(), this.getTanggal_masuk());
        if(!statement.execute(Query)){
            JOptionPane.showMessageDialog(null, "Berhasil Disimpan");
        }
        resetForm();
    }
    
    public void updateHutang() throws SQLException{
        int hutang = this.getSisa_hutang();
        int bayar = Integer.parseInt(this.getBayar());
        String status = this.getStatus();
        String peminjaman_id = this.getPeminjaman_id();
        int sisa_hutang = hutang - bayar;
            
        if(sisa_hutang == 0){
            status = "Lunas";
        }
  
        String Query = "UPDATE peminjaman SET sisa_hutang = '%d', status = '%s' WHERE peminjaman_id = '%s'";
        Query = String.format(Query, sisa_hutang, status, this.getPeminjaman_id());
        if(!statement.execute(Query)){
            JOptionPane.showMessageDialog(null, "Berhasil Diupdate");
        }
        resetForm();
    }
}
