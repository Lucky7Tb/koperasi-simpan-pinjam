/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package koperasi.model;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Date;
import javax.swing.table.DefaultTableModel;
import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.util.Set;
import java.util.TreeMap;

import static koperasi.database.DatabaseConnection.result;
import static koperasi.database.DatabaseConnection.statement;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

/**
 *
 * @author Luckyfe
 */
public class ReportModel {
     DefaultTableModel model = new DefaultTableModel();
     SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
     Date date = new Date();
     
     LocalDate currentDate = LocalDate.now();
     LocalDate weekAgo = currentDate.minus(1, ChronoUnit.WEEKS);
     
     public ReportModel(){
        model.addColumn("Nama");
        model.addColumn("Jumlah uang");
        model.addColumn("Bunga");
        model.addColumn("Jangka waktu");
        model.addColumn("Bayaran perbulan");
        model.addColumn("Sisa hutang");
        model.addColumn("Tanggal pinjam"); 
        model.addColumn("Status"); 
     }
     
    public DefaultTableModel loadReportHarianTable() throws SQLException{
        String Query = "SELECT member.member_name, peminjaman.jumlah_uang, peminjaman.bunga, peminjaman.jangka_waktu, peminjaman.uang_per_bulan, peminjaman.sisa_hutang, peminjaman.created_at, peminjaman.status FROM peminjaman INNER JOIN member ON peminjaman.member_id = member.member_id WHERE created_at = '%s'";
        Query = String.format(Query, dateFormat.format(date));
        result = statement.executeQuery(Query);
        model.setNumRows(0);
        while(result.next()){
            model.addRow(new Object[]{result.getString(1),result.getString(2),result.getString(3),result.getString(4),result.getString(5),result.getString(6),result.getString(7),result.getString(8)});
        }
        return model;
    }
    
    public DefaultTableModel loadReportMingguanTable() throws SQLException{
        String Query = "SELECT member.member_name, peminjaman.jumlah_uang, peminjaman.bunga, peminjaman.jangka_waktu, peminjaman.uang_per_bulan, peminjaman.sisa_hutang, peminjaman.created_at, peminjaman.status FROM peminjaman INNER JOIN member ON peminjaman.member_id = member.member_id WHERE created_at BETWEEN '%s' AND '%s'";
        Query = String.format(Query, weekAgo, currentDate);
        result = statement.executeQuery(Query);
        model.setNumRows(0);
        while(result.next()){
            model.addRow(new Object[]{result.getString(1),result.getString(2),result.getString(3),result.getString(4),result.getString(5),result.getString(6),result.getString(7),result.getString(8)});
        }
        return model;
    }
    
    public DefaultTableModel loadReportTahunanTable() throws SQLException{
        String Query = "SELECT member.member_name, peminjaman.jumlah_uang, peminjaman.bunga, peminjaman.jangka_waktu, peminjaman.uang_per_bulan, peminjaman.sisa_hutang, peminjaman.created_at, peminjaman.status FROM peminjaman INNER JOIN member ON peminjaman.member_id = member.member_id WHERE YEAR(created_at) = '%s' ";
        Query = String.format(Query, currentDate.getYear());
        result = statement.executeQuery(Query);
        model.setNumRows(0);
        while(result.next()){
            model.addRow(new Object[]{result.getString(1),result.getString(2),result.getString(3),result.getString(4),result.getString(5),result.getString(6),result.getString(7),result.getString(8)});
        }
        return model;
    }
    
    public String getCellValue(int x, int y){
        return model.getValueAt(x, y).toString();
    }
    
    public void exportToExcel() throws IOException{
        XSSFWorkbook WorkBook = new XSSFWorkbook();
        XSSFSheet Sheet = WorkBook.createSheet();
        
        TreeMap<String, Object[]> data = new TreeMap<>();
       
        data.put("-1", new Object[]{model.getColumnName(0),model.getColumnName(1),model.getColumnName(2),model.getColumnName(3),model.getColumnName(4),model.getColumnName(5),model.getColumnName(6),model.getColumnName(7)});
        
        for(int i = 0; i < model.getRowCount(); i++){
            data.put(Integer.toString(i), new Object[]{ getCellValue(i,0), getCellValue(i,1), getCellValue(i,2), getCellValue(i,3), getCellValue(i,4), getCellValue(i,5), getCellValue(i,6), getCellValue(i,7) });      
        }
        
        Set<String> ids = data.keySet();
        XSSFRow row;
        
        int rowID = 0;
        
        for(String key: ids){
            row = Sheet.createRow(rowID++);
            
            Object[] values = data.get(key);
            
            int cellID = 0;
            
            for(Object object: values){
                Cell cell = row.createCell(cellID++);
                cell.setCellValue(object.toString());
            }
          
        }
        
       
            FileOutputStream file = new FileOutputStream(new File("D:\\Excel\\report.xlsx"));
            
            WorkBook.write(file);
            
            file.close();
       
    }

}
