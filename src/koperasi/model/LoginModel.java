/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package koperasi.model;

import java.sql.SQLException;
import javax.swing.JOptionPane;
import koperasi.event.LoginListener;
import static koperasi.database.DatabaseConnection.result;
import static koperasi.database.DatabaseConnection.statement;
import koperasi.view.LoginView;
import koperasi.view.MainFrame;
/**
 *
 * @author USER
 */
public class LoginModel {
    private String email;
    private String password;

    
    private LoginListener loginListener;
    
    public LoginListener getLoginListener(){
        return loginListener;
    }
    
    public void setLoginListener(LoginListener loginListener){
        this.loginListener = loginListener;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
        fireOnChange();
    }
    
    public void setPassword(String password) {
        this.password = password;
        fireOnChange();
    }

    public String getPassword() {
        return password;
    }

 
    protected void fireOnChange(){
        if(loginListener != null){
            loginListener.onChange(this);
        }
    }
    
    public void resetForm(){
        setEmail("");
        setPassword("");
    }
    
    public void login(LoginView view, MainFrame main) throws SQLException{
        String Query = "SELECT * FROM admin WHERE admin_email = '%s' AND admin_password = '%s'";
        Query = String.format(Query, this.getEmail(), this.getPassword());
        result = statement.executeQuery(Query);
        while(result.next()){
            System.out.println("Ada data");
            view.dispose();
             new MainFrame().setVisible(true);
        }
        resetForm();
    }
}
