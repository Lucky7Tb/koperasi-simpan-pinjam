-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               10.4.6-MariaDB - mariadb.org binary distribution
-- Server OS:                    Win64
-- HeidiSQL Version:             10.2.0.5599
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


-- Dumping database structure for koperasi
CREATE DATABASE IF NOT EXISTS `koperasi` /*!40100 DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci */;
USE `koperasi`;

-- Dumping structure for table koperasi.admin
CREATE TABLE IF NOT EXISTS `admin` (
  `admin_id` varchar(6) COLLATE utf8mb4_unicode_ci NOT NULL,
  `admin_name` varchar(80) COLLATE utf8mb4_unicode_ci NOT NULL,
  `admin_email` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `admin_phone` varchar(15) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `admin_address` varchar(80) COLLATE utf8mb4_unicode_ci NOT NULL,
  `admin_password` varchar(80) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` date NOT NULL DEFAULT curdate(),
  PRIMARY KEY (`admin_id`),
  UNIQUE KEY `admin_email` (`admin_email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table koperasi.admin: ~1 rows (approximately)
DELETE FROM `admin`;
/*!40000 ALTER TABLE `admin` DISABLE KEYS */;
INSERT INTO `admin` (`admin_id`, `admin_name`, `admin_email`, `admin_phone`, `admin_address`, `admin_password`, `created_at`) VALUES
	('pfuoh', 'Lucky', 'luckytribhakti@gmail.com', '089946839', 'Bandung', '12345678', '2019-11-28'),
	('sddfc', 'admin', 'admin@mail.com', '089981882', 'Bandung', 'admin', '2019-12-08');
/*!40000 ALTER TABLE `admin` ENABLE KEYS */;

-- Dumping structure for table koperasi.member
CREATE TABLE IF NOT EXISTS `member` (
  `member_id` varchar(6) COLLATE utf8mb4_unicode_ci NOT NULL,
  `member_name` varchar(80) COLLATE utf8mb4_unicode_ci NOT NULL,
  `member_phone` varchar(15) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `member_email` varchar(80) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `member_address` varchar(80) COLLATE utf8mb4_unicode_ci NOT NULL,
  `member_join_date` date NOT NULL DEFAULT curdate(),
  PRIMARY KEY (`member_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table koperasi.member: ~2 rows (approximately)
DELETE FROM `member`;
/*!40000 ALTER TABLE `member` DISABLE KEYS */;
/*!40000 ALTER TABLE `member` ENABLE KEYS */;

-- Dumping structure for table koperasi.peminjaman
CREATE TABLE IF NOT EXISTS `peminjaman` (
  `peminjaman_id` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `member_id` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `jumlah_uang` int(255) NOT NULL,
  `bunga` int(255) NOT NULL,
  `jangka_waktu` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `uang_per_bulan` int(255) NOT NULL DEFAULT 0,
  `sisa_hutang` int(255) NOT NULL DEFAULT 0,
  `created_at` date NOT NULL DEFAULT curdate(),
  `status` enum('Lunas','Belum Lunas') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'Belum Lunas',
  PRIMARY KEY (`peminjaman_id`),
  KEY `member_id` (`member_id`),
  CONSTRAINT `FK_PEMINJAMAN_MEMBER_ID` FOREIGN KEY (`member_id`) REFERENCES `member` (`member_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table koperasi.peminjaman: ~1 rows (approximately)
DELETE FROM `peminjaman`;
/*!40000 ALTER TABLE `peminjaman` DISABLE KEYS */;
/*!40000 ALTER TABLE `peminjaman` ENABLE KEYS */;

-- Dumping structure for table koperasi.simpanan_pokok
CREATE TABLE IF NOT EXISTS `simpanan_pokok` (
  `simpanan_pokok_id` varchar(6) COLLATE utf8mb4_unicode_ci NOT NULL,
  `member_id` varchar(6) COLLATE utf8mb4_unicode_ci NOT NULL,
  `simpanan_pokok` int(255) NOT NULL,
  `created_at` date NOT NULL DEFAULT curdate(),
  PRIMARY KEY (`simpanan_pokok_id`),
  KEY `member_id` (`member_id`),
  CONSTRAINT `FK_SP_MEMBER_ID` FOREIGN KEY (`member_id`) REFERENCES `member` (`member_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table koperasi.simpanan_pokok: ~2 rows (approximately)
DELETE FROM `simpanan_pokok`;
/*!40000 ALTER TABLE `simpanan_pokok` DISABLE KEYS */;
/*!40000 ALTER TABLE `simpanan_pokok` ENABLE KEYS */;

-- Dumping structure for table koperasi.simpanan_sukarela
CREATE TABLE IF NOT EXISTS `simpanan_sukarela` (
  `simpanan_sukarela_id` varchar(6) COLLATE utf8mb4_unicode_ci NOT NULL,
  `member_id` varchar(6) COLLATE utf8mb4_unicode_ci NOT NULL,
  `simpanan_sukarela` int(255) NOT NULL,
  `created_at` date NOT NULL DEFAULT curdate(),
  PRIMARY KEY (`simpanan_sukarela_id`),
  KEY `member_id` (`member_id`),
  CONSTRAINT `FK_SS_MEMBER_ID` FOREIGN KEY (`member_id`) REFERENCES `member` (`member_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table koperasi.simpanan_sukarela: ~2 rows (approximately)
DELETE FROM `simpanan_sukarela`;
/*!40000 ALTER TABLE `simpanan_sukarela` DISABLE KEYS */;
/*!40000 ALTER TABLE `simpanan_sukarela` ENABLE KEYS */;

-- Dumping structure for table koperasi.simpanan_wajib
CREATE TABLE IF NOT EXISTS `simpanan_wajib` (
  `simpanan_wajib_id` varchar(6) COLLATE utf8mb4_unicode_ci NOT NULL,
  `member_id` varchar(6) COLLATE utf8mb4_unicode_ci NOT NULL,
  `simpanan_wajib` int(255) NOT NULL DEFAULT 750000,
  `created_at` date NOT NULL DEFAULT curdate(),
  PRIMARY KEY (`simpanan_wajib_id`),
  KEY `member_id` (`member_id`),
  CONSTRAINT `FK_SW_MEMBER_ID` FOREIGN KEY (`member_id`) REFERENCES `member` (`member_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table koperasi.simpanan_wajib: ~2 rows (approximately)
DELETE FROM `simpanan_wajib`;
/*!40000 ALTER TABLE `simpanan_wajib` DISABLE KEYS */;
/*!40000 ALTER TABLE `simpanan_wajib` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
